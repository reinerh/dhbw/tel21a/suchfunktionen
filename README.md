# Suchfunktionen

Aufgaben, bei denen es um die Suche von Einträgen in verschiedenartigen Listen geht.

## Aufgabe 1:

Implementieren Sie die Funktion ```find()```.
Die Funktion soll eine Liste von Strings und einen einzelnen String erwarten.
Sie soll die Position in der Liste liefern, an der der einzelne String vorkommt.

**Hinweise:**
* Überlegen Sie sich, was die Funktion liefern soll,
  falls der gesuchte String nicht in der Liste vorkommt.

## Aufgabe 2:

Implementieren Sie die Funktion ```lookup()```.
Die Funktion soll zwei Listen von Strings und einen einzelnen String erwarten.
Sie soll den einzelnen String in der ersten Liste suchen und 
das entsprechende Element aus der zweiten Liste liefern.

**Hinweise:**
* Überlegen Sie sich, was die Funktion liefern soll,
  falls der gesuchte String nicht in der ersten Liste vorkommt.

## Aufgabe 3:

Implementieren Sie mit Hilfe der Funktion ```lookup()``` ein einfaches Deutsch-Englisch-Wörterbuch:
Schreiben Sie eine Funktion, die den Benutzer nach einem deutschen Wort fragt und das entsprechende englische liefert.

**Hinweise:**
* Sie können die Hilfsfunktion ```createDictionary()``` verwenden und modifizieren, um eine Datenbasis zu erzeugen. 

## Aufgabe 4:

Beschreiben Sie möglichst allgemein, was ein "_Eintrag_" in einem Wörterbuch ist:
Was für Datensätze ergeben zusammengefasst einen Eintrag.

Suchen Sie im Internet nach den Begriffen "golang struct" und versuchen Sie,
mit Hilfe der gefundenen Ergebnisse einen Datentyp für Wörterburcheinträge zu definieren.
Schreiben Sie die Funktionen ```find()```, ```lookup()``` etc. so um, dass sie
mit diesem neuen Ansatz arbeiten.

## Aufgabe 5:

Erweitern Sie Ihre Datenstruktur (oder den vorgegebenen Ansatz mit mehreren Listen),
so dass man einem einzelnen deutschen Wort mehrere englische Wörter zuordnen kann.