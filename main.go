package main

func main() {

}

// Erwartet eine Liste von Strings und einen einzelnen String.
// Liefert die Position in der Liste, an der der einzelne String vorkommt.
func find(list []string, word string) int {
	// TODO: Funktion implementieren.
	// TODO: Entscheiden, was geliefert wird, wenn word nicht in list vorkommt.

	return 0 // TODO
}

// Erwartet zwei Listen von Strings und einen einzelnen String.
// Sucht den einzelnen String in der ersten Liste und liefert
// den entsprechenden Eintrag aus der zweiten Liste.
func lookup(list1, list2 []string, word string) string {
	// TODO: Funktion implementieren
	// TODO: Entscheiden, was geliefert wird, wenn word nicht in list1 vorkommt.

	return "" // TODO
}

// Liefert zwei Listen mit deutschen und englischen Wörtern.
func createDictonary() ([]string, []string) {
	de := []string{"Hund", "Katze", "Maus", "Auto", "Fahrrad", "Flugzeug"}
	en := []string{"dog", "cat", "mouse", "car", "bicycle", "airplane"}

	return de, en
}
